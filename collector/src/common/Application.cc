/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2021, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors:                                                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "collector/Application.h"
#include "b2in/nub/Method.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "b2in/nub/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#define PSX_NS_URI "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd"


XDAQ_INSTANTIATOR_IMPL (collector::Application)

collector::Application::Application(xdaq::ApplicationStub* stub): xdaq::Application(stub), xgi::framework::UIManager(this), eventing::api::Member(this)
{
    stub->getDescriptor()->setAttribute("icon","/collector/images/collector.png");

    eventingBusName_ = "mtdbus";
    
    getApplicationInfoSpace()->fireItemAvailable("eventingBusName", &eventingBusName_);

    b2in::nub::bind(this, &collector::Application::onMessage);

    xgi::framework::deferredbind(this, this, &collector::Application::Default, "Default");

    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void collector::Application::actionPerformed(xdata::Event& event)
{
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
        try
        {
            this->getEventingBus(eventingBusName_).subscribe("atopic");
        }
        catch (eventing::api::exception::Exception & e)
        {
            this->notifyQualified("error",e);
        }
    }
}

// Callback function invoked for the b2in token message
// that is received from the Clients
//
void collector::Application::onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist)
{
    if (ref != 0)
    {
        ref->release();
    }
    
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action != "notify")
    {
       // ignore 
       return;
    }
    // Example properties
    // counter value "some value"
    // urn:b2in-eventing:action value notify
    // urn:b2in-eventing:context value 101
    // urn:b2in-eventing:id value ed14cf9a-98f8-47c3-a92b-2403e9f8d15f
    // urn:b2in-eventing:topic value atopic
    // urn:b2in-protocol-tcp:connection value keep-alive
    // urn:b2in-protocol-tcp:connection-timeout value 5
    // urn:b2in-protocol-tcp:destination value b2in+utcp://10.176.142.129:1911
    // urn:b2in-protocol-tcp:originator value utcp://10.176.142.130:1977
    // urn:b2in-protocol:lid value 67
    // urn:b2in-protocol:originator value http://kvm-s3562-1-ip151-69.cms:4002
    //
    LOG4CPLUS_DEBUG(getApplicationLogger(), "received notification");
    for ( std::map<std::string, std::string, std::less<std::string>>::iterator i = plist.begin(); i != plist.end(); i++ )
    {
    	LOG4CPLUS_DEBUG(getApplicationLogger(), "received property " << (*i).first << " value " << (*i).second );
    }
        
    this->dpSet("TestDP_In.:_original.._value", plist.getProperty("value"));
    
}

void collector::Application::dpSet(const std::string & dp, const std::string & value)
{
        
    // constructed using XOAP
    xoap::MessageReference msg = xoap::createMessage();
    
    try
    {
        xoap::SOAPPart soap = msg->getSOAPPart();
        xoap::SOAPEnvelope env = soap.getEnvelope();
        xoap::SOAPBody body = env.getBody();
        xoap::SOAPName cmdName = env.createName("dpSet","psx",PSX_NS_URI);
        xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
        
        // add data point name: <psx::dp name="name">value</psx::dp>
        xoap::SOAPName dpName =  env.createName("dp","psx",PSX_NS_URI);
        xoap::SOAPElement dpElement = bodyElem.addChildElement(dpName);
        xoap::SOAPName nameAttribute = env.createName("name","","");
        dpElement.addAttribute(nameAttribute, dp);
        dpElement.addTextNode(value);
    }
    catch(xoap::exception::Exception& xe)
    {
        XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);
    }
    
    // Send SOAP message
    try
    {
        const xdaq::ApplicationDescriptor * d = this->getApplicationContext()->getDefaultZone()->getApplicationDescriptor("psx::Application", 0);
        const xdaq::ApplicationDescriptor * s = getApplicationDescriptor();
        xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);
        
        // here can check for error if any to be reported on sentinel
        
        //reply->writeTo(std::cout);
        //std::cout << std::endl;
    }
    catch (xdaq::exception::Exception& e)
    {
        XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);
    }
        
}

std::string collector::Application::dpGet(const std::string & dp)
{
	xoap::MessageReference msg = xoap::createMessage();

	try
	{
        	xoap::SOAPPart soap = msg->getSOAPPart();
        	xoap::SOAPEnvelope env = soap.getEnvelope();
		xoap::SOAPBody body = env.getBody();
		xoap::SOAPName cmdName = env.createName("dpGet","psx",PSX_NS_URI);
		xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
		xoap::SOAPName dpName =  env.createName("dp","psx",PSX_NS_URI);
		xoap::SOAPElement dpElement = bodyElem.addChildElement(dpName);
		xoap::SOAPName nameAttribute = env.createName("name","","");
		dpElement.addAttribute(nameAttribute, dp);
	}
	catch(xoap::exception::Exception& xe)
	{
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
	}

	try
	{	
		const xdaq::ApplicationDescriptor * d = this->getApplicationContext()->getDefaultZone()->getApplicationDescriptor("psx::Application", 0);
        	const xdaq::ApplicationDescriptor * s = getApplicationDescriptor();

		xoap::MessageReference reply = this->getApplicationContext()->postSOAP(msg, *s, *d);
		std::vector<xoap::SOAPElement> relements = reply->getSOAPPart().getEnvelope().getBody().getChildElements();
 		std::vector<xoap::SOAPElement> dpelements = relements[0].getChildElements();
		std::string value = dpelements[0].getTextContent();

		//reply->writeTo(std::cout);
		//std::cout << std::endl;
		return value;
	} 
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
	}
}

void collector::Application::Default(xgi::Input* in, xgi::Output* out)
{
    *out << "<div class=\"xdaq-tab-wrapper\">";
    *out << "<div class=\"xdaq-tab\" title=\"Statistics\">";
    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Data points\">";
    *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::caption("Data Points");
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::th();
    *out << "TestDP_Out";
    *out << cgicc::th();
    *out << cgicc::td();

    *out << this->dpGet("TestDP_Out.:_original.._value");

    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::table() << std::endl;

    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Eventing\">";
        
    *out << "<h3>Buses</h3>";
        
    *out << this->busesToHTML() << std::endl;
        
    *out << "</div>";
    *out << "</div>";
}
