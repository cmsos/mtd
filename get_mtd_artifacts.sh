#!/bin/bash -x

GITLAB_URL="https://gitlab.cern.ch"
GITLAB_ARTIFACT_TOKEN="zmBFS9UFpRroJw6c-bRg"
group="cmsos"
project="mtd"
projectid="110189"
branch="feature_1_skeleton"
#jobid="12059701"
#jobid=$1

outZipFile="$project.zip"
outHeadersFile="$outZipFile.httpheaders"

rm -rf rpm
rm -rf "$outZipFile"
rm -rf "$outHeadersFile"

#response=$(curl "$GITLAB_URL/api/v4/projects/${group}%2F${project}/jobs/artifacts/$branch/download?job=$job" \
#response=$(curl "$GITLAB_URL/api/v4/projects/${projectid}/jobs/${jobid}/artifacts" \
response=$(curl "$GITLAB_URL/api/v4/projects/${projectid}/jobs/artifacts/${branch}/download?job=all" \
  --location \
  -w "%{http_code}\n" \
  -D "$outHeadersFile" \
  -o "$outZipFile.tmp" \
  --header "PRIVATE-TOKEN: $GITLAB_ARTIFACT_TOKEN")

if [[ "$response" == 4* ]] || [[ "$response" == 5* ]]; then
  echo "ERROR - Http status: $response"
  rm "$outZipFile.tmp"
  exit 1
elif [[ "$response" == 304 ]]; then
  echo "$project is up-to-date"
else
  echo "update $outZipFile"
  mv "$outZipFile.tmp" "$outZipFile"
  unzip "$outZipFile"
fi

