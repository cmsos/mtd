/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2021, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors:                                                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _configurator_application_h_
#define _configurator_application_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

#include "xdata/ActionListener.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"

namespace configurator
{
	class Application: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
	{
		public:
			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* stub);

			//
			// Callback for requesting current exported parameter values
			//
			void actionPerformed(xdata::Event& e);

			// Web callback functions
			void Default(xgi::Input* in, xgi::Output* out);
            void config(xgi::Input* in, xgi::Output* out);


	};
}

#endif
