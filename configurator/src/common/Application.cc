/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2021, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors:                                                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "configurator/Application.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "xoap/SOAPConstants.h"

XDAQ_INSTANTIATOR_IMPL (configurator::Application)

configurator::Application::Application(xdaq::ApplicationStub* stub): xdaq::Application(stub), xgi::framework::UIManager(this)
{
    stub->getDescriptor()->setAttribute("icon","/configurator/images/configurator.png");
    xgi::framework::deferredbind(this, this, &configurator::Application::Default, "Default");
    xgi::bind(this, &configurator::Application::config, "config");

    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void configurator::Application::actionPerformed(xdata::Event& event)
{
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
    }
}

void configurator::Application::config(xgi::Input* in, xgi::Output* out)
{
    // Retrieve the value string
    std::string value = "";
    std::string searchKey = "";
    try
    {
        cgicc::Cgicc cgi(in);
        value = cgi["val"]->getValue();
    }
    catch (std::exception& e)
    {
        std::string msg = "An error occurred while interpreting the form content: ";
        msg += e.what();
        XCEPT_RAISE (xgi::exception::Exception, msg);
    }
    
    std::set<const xdaq::ApplicationDescriptor*> destinations = this->getApplicationContext()->getDefaultZone()->getApplicationDescriptors("controller::Application");
    for(std::set<const xdaq::ApplicationDescriptor*>::iterator iter = destinations.begin(); iter !=destinations.end(); iter++)
    {
        
        xoap::MessageFactory * factory = xoap::MessageFactory::getInstance(xoap::SOAPConstants::SOAP_1_2_PROTOCOL);
        xoap::MessageReference msg = factory->createMessage();
        xoap::SOAPPart soap = msg->getSOAPPart();
        xoap::SOAPEnvelope envelope = soap.getEnvelope();
        xoap::SOAPBody body = envelope.getBody();
        xoap::SOAPName command = envelope.createName("onMessage","xdaq", "urn:xdaq-soap:3.0");
        body.addBodyElement(command);
        
        // add an attachment with generated (and in this case invalid) data
        xoap::AttachmentPart * attachment = msg->createAttachmentPart(value.c_str(), value.size(), "text/plain");
        toolbox::net::URL address(getApplicationContext()->getContextDescriptor()->getURL());
        msg->addAttachmentPart(attachment);

        try
        {
            const xdaq::ApplicationDescriptor * s = getApplicationDescriptor();
            xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *(*iter));
        }
        catch (xdaq::exception::Exception& e)
        {
           this->notifyQualified("error", e);
        }
    }
    
    this->getHTMLHeader(in, out);
    this->Default(in,out);
    this->getHTMLFooter(in, out);

}

void configurator::Application::Default(xgi::Input* in, xgi::Output* out)
{
    std::string url = "/";
    url += getApplicationDescriptor()->getURN();
    url += "/config";
    *out << cgicc::form().set("method","post").set("action", url).set("enctype","multipart/form-data") << std::endl;
    *out << cgicc::label("Value") << std::endl;
    *out << cgicc::input().set("type", "text").set("name", "val").set("size","35") << std::endl;
    *out << cgicc::br() << std::endl;
    *out << cgicc::br() << std::endl;
    *out << cgicc::input().set("type", "submit").set("name", "config").set("value", "config") << std::endl;;
    *out << cgicc::form() << std::endl;

}
