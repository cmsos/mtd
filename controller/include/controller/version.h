/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2021, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors:                                                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _controller_version_h_
#define _controller_version_h_

#include "config/PackageInfo.h"

#define MTD_CONTROLLER_VERSION_MAJOR 1
#define MTD_CONTROLLER_VERSION_MINOR 0
#define MTD_CONTROLLER_VERSION_PATCH 0
// If any previous versions available E.g. #define MTD_CONTROLLER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef MTD_CONTROLLER_PREVIOUS_VERSIONS


#define MTD_CONTROLLER_VERSION_CODE PACKAGE_VERSION_CODE(MTD_CONTROLLER_VERSION_MAJOR,MTD_CONTROLLER_VERSION_MINOR,MTD_CONTROLLER_VERSION_PATCH)
#ifndef MTD_CONTROLLER_PREVIOUS_VERSIONS
#define MTD_CONTROLLER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(MTD_CONTROLLER_VERSION_MAJOR,MTD_CONTROLLER_VERSION_MINOR,MTD_CONTROLLER_VERSION_PATCH)
#else
#define MTD_CONTROLLER_FULL_VERSION_LIST MTD_CONTROLLER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(MTD_CONTROLLER_VERSION_MAJOR,MTD_CONTROLLER_VERSION_MINOR,MTD_CONTROLLER_VERSION_PATCH)
#endif
namespace controller
{
	const std::string project = "mtd";
	const std::string package = "controller";
	const std::string versions = MTD_CONTROLLER_FULL_VERSION_LIST;
	const std::string summary = "MTD controller";
	const std::string description = "MTD controller";
	const std::string authors = "";
	const std::string link = "https://gitlab.cern.ch/cmsos/mtd";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
