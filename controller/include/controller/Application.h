/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2021, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors:                                                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _controller_application_h_
#define _controller_application_h_

#include "toolbox/ActionListener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/BSem.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/NamespaceURI.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

#include "xdata/ActionListener.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger64.h"

#include "eventing/api/Member.h"

namespace controller
{
	class Application: public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener ,public eventing::api::Member
	{
		public:
			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* stub);

			//
			// Callback for requesting current exported parameter values
			//
			void actionPerformed(xdata::Event& e);

			void actionPerformed (toolbox::Event& e);

			// Web callback functions
			void Default(xgi::Input* in, xgi::Output* out);
			void test(xgi::Input *in, xgi::Output *out);
        
            xoap::MessageReference onMessage (xoap::MessageReference msg);
        
        protected:
            void timeExpired (toolbox::task::TimerEvent& e);

        
        
            xdata::String eventingBusName_;
        
            toolbox::BSem mutex_;
        
            xdata::UnsignedInteger64 publishCounter_;

	   // fake hardware register
	   std::string temperature_;

	};
}

#endif
