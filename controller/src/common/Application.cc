/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2021, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors:                                                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "controller/Application.h"
#include "b2in/nub/Method.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/task/Guard.h"



XDAQ_INSTANTIATOR_IMPL (controller::Application)

controller::Application::Application(xdaq::ApplicationStub* stub): xdaq::Application(stub), xgi::framework::UIManager(this), eventing::api::Member(this), mutex_(toolbox::BSem::FULL)
{
    stub->getDescriptor()->setAttribute("icon","/controller/images/controller.png");
    eventingBusName_ = "mtdbus";
    
    publishCounter_ = 0;
    
    temperature_ = "0"; // a fake hardware register
    
    getApplicationInfoSpace()->fireItemAvailable("eventingBusName", &eventingBusName_);

    xgi::framework::deferredbind(this, this, &controller::Application::Default, "Default");
    xgi::bind(this, &controller::Application::test, "test");
    
    //
    // Bind SOAP callback
    //
    xoap::bind(this, &controller::Application::onMessage, "onMessage", XDAQ_NS_URI );

    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void controller::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "eventing::api::BusReadyToPublish")
	{
		std::string busname = (static_cast<eventing::api::Bus*>(event.originator()))->getBusName();
		std::cout << "event Bus '" << busname << "' is ready to publish"  << std::endl;
	}
}

void controller::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		// User should listen for publsh readiness, but should also check immediately
		this->getEventingBus(eventingBusName_).addActionListener(this);
		if (this->getEventingBus(eventingBusName_).canPublish())
		{
			// ready now
			std::cout << "Eventing bus is ready at setDefaultValues" << std::endl;
		}
        
        toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("mtd");
        toolbox::TimeInterval interval;
                     
        interval.fromString("PT5S");
        toolbox::TimeVal start;
        start = toolbox::TimeVal::gettimeofday();
        timer->scheduleAtFixedRate(start, this, interval, 0, "mtd-staging");
        
	}
}

// periodically get temperature and publish
void controller::Application::timeExpired (toolbox::task::TimerEvent& e)
{
        toolbox::task::Guard < toolbox::BSem > guard(mutex_);
    
        std::string name = e.getTimerTask()->name;

        if (name == "mtd-staging")
        {
                // check and read hardware (fake)
                std::string value = temperature_;
                // publish
                // create fake message with fake property
                xdata::Properties plist;
                plist.setProperty("value", value);

                this->getEventingBus(eventingBusName_).publish("atopic", 0, plist);
                
                publishCounter_++;
        }
}

// from configurator
xoap::MessageReference controller::Application::onMessage (xoap::MessageReference msg)
{
    toolbox::task::Guard < toolbox::BSem > guard(mutex_);
    
    //std::string message;
    //msg->writeTo(message);
    //std::cout << "msg = " << message << std::endl;

    
    std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
    xoap::AttachmentPart* ap = *(attachments.begin());
    std::string value(ap->getContent(),ap->getSize());
    
    
    // something to be done to configure hw
    temperature_ = value; 
    
    
    // reply to caller
    xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = envelope.createName( "onMessageResponse", "xdaq", XDAQ_NS_URI);
    xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
    return reply;
}

// test function for browser or curl etc.
void controller::Application::test (xgi::Input * in, xgi::Output * out)
{
	// create fake message with fake property
	xdata::Properties plist;
	plist.setProperty("value", temperature_);

	this->getEventingBus(eventingBusName_).publish("atopic", 0, plist);

	std::cout << "message sent" << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}


void controller::Application::Default(xgi::Input* in, xgi::Output* out)
{
    toolbox::task::Guard < toolbox::BSem > guard(mutex_);
    
    *out << "<div class=\"xdaq-tab-wrapper\">";
    *out << "<div class=\"xdaq-tab\" title=\"Hardware Display\">";
    
    // something to be displayed from HW
    *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::caption("Temperature");
    *out << cgicc::tbody() << std::endl;
    
    // Exceptions sent
    //
    *out << cgicc::tr();
    *out << cgicc::th();
    *out << "Temperature";
    *out << cgicc::th();
    *out << cgicc::td();
    
    *out << temperature_;
    
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::table() << std::endl;
    
    
    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Statistics\">";
    *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::caption("Network Status");
    *out << cgicc::tbody() << std::endl;
    
    // Exceptions sent
    //
    *out << cgicc::tr();
    *out << cgicc::th();
    *out << "Total publish";
    *out << cgicc::th();
    *out << cgicc::td();
    
    *out << publishCounter_.toString();
    
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::table() << std::endl;
    
    // something to be displaed here
    
    *out << "</div>";
    *out << "<div class=\"xdaq-tab\" title=\"Eventing\">";
    
    *out << "<h3>Buses</h3>";
    
    *out << this->busesToHTML() << std::endl;
    
    *out << "</div>";
    *out << "</div>";
}
